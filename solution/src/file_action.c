#include "../include/file_action.h"
#include "../include/bmp.h"

enum open_file_status open_file(char const *name, struct image *image)
{
    if (!name)
        return OPEN_INVALID_FILENAME;
    FILE *in = fopen(name, "rb");
    if (!in)
        return OPEN_ERROR;
    enum read_status read_status_v = from_bmp(in, image);
    fclose(in);
    return (read_status_v != READ_OK) ? OPEN_ERROR : OPEN_OK;
}

enum close_file_status close_file(char const *name, struct image *image)
{
    FILE *out = fopen(name, "wb");
    if (!out)
        return CLOSE_ERROR;
    enum write_status write_status_v = to_bmp(out, image);
    fclose(out);
    return (write_status_v != WRITE_OK) ? CLOSE_ERROR : CLOSE_OK;
}
