#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
// copy form tests

_Noreturn void err(const char *msg, ...)
{
    va_list args;
    va_start(args, msg);
    vfprintf(stderr, msg, args); // NOLINT
    va_end(args);
    exit(1);
}
