#include "bmp.h"
#include <stdlib.h>

static enum read_status bmp_validate(const struct bmp_header *const head)
{
    if (head->bfType != BMP_VALID_BFTYPE || (head->biSizeImage && (head->bfileSize != (head->bOffBits + head->biSizeImage))) || head->biPlanes != BMP_VALID_PLANS || head->biBitCount != BMP_VALID_BITCOUNT || head->biCompression != BMP_VALID_COMPRESSION)
        return READ_INVALID_HEADER;
    return READ_OK;
}

/*  deserializer */
enum read_status from_bmp(FILE *const file_in, struct image *img)
{
    // create new header
    struct bmp_header bmp_head = (struct bmp_header){0};
    // how many byte?
    size_t header_bytes = fread(&bmp_head, sizeof(struct bmp_header), 1, file_in);
    if (header_bytes < 1)
    {

        return READ_INVALID_HEADER;
    }
    // check header
    enum read_status header_read_status = bmp_validate(&bmp_head);
    if (header_read_status != READ_OK)
        return header_read_status;
    if (fseek(file_in, bmp_head.bOffBits, SEEK_SET))
        return READ_INVALID_SIGNATURE;

    // create new image, used to store bmp image
    struct dimensions size = {.x = bmp_head.biWidth, .y = bmp_head.biHeight};
    *img = image_create(size);

    uint8_t horizontal_offset = 4 - (img->size.x * sizeof(struct pixel) % 4); // 1,2,3
    // clone data from file
    for (size_t row = 0 /*img->size.y - 1*/; row < img->size.y; row++)
    {
        size_t bitmap_bytes_read = fread(img->data + row * img->size.x, sizeof(struct pixel), img->size.x, file_in);
        if (bitmap_bytes_read < img->size.x)
        {
            image_destroy(img);
            return READ_INVALID_BITS;
        }

        if (fseek(file_in, horizontal_offset, SEEK_CUR))
        {
            image_destroy(img);
            return READ_INVALID_BITS;
        }
    }

    return READ_OK;
}

static struct bmp_header create_valid_bmp_header(const struct image *const img)
{
    const uint32_t size_image = img->size.y * (img->size.x * sizeof(struct pixel) + img->size.x % 4);
    const size_t offbit = sizeof(struct bmp_header);

    return (struct bmp_header){
        .bfType = BMP_VALID_BFTYPE,
        .bfileSize = offbit + size_image,
        .bfReserved = BMP_VALID_RESERVED,
        .bOffBits = offbit,
        .biSize = BMP_VALID_SIZE,
        .biWidth = img->size.x,
        .biHeight = img->size.y,
        .biPlanes = BMP_VALID_PLANS,
        .biBitCount = BMP_VALID_BITCOUNT,
        .biCompression = BMP_VALID_COMPRESSION,
        .biSizeImage = size_image,
        .biXPelsPerMeter = BMP_VALID_XPELS,
        .biYPelsPerMeter = BMP_VALID_YPELS,
        .biClrUsed = BMP_VALID_CLRU,
        .biClrImportant = BMP_VALID_CLRI};
}

/*  serializer   */
enum write_status to_bmp(FILE *const file_out, struct image *const img)
{
    static uint8_t offset_buffer[] = {0, 0, 0};

    struct bmp_header out_bmp_head = (struct bmp_header){0};
    out_bmp_head = create_valid_bmp_header(img);

    if (fwrite(&out_bmp_head, sizeof(struct bmp_header), 1, file_out) < 1)
    {
        image_destroy(img);
        return WRITE_ERROR;
    }
    uint8_t horizontal_offset = (4 - (img->size.x * sizeof(struct pixel) % 4)) % 4; // 1,2,3
    for (size_t row = 0; row < img->size.y; row++)
    {
        if (fwrite(img->data + row * img->size.x, sizeof(struct pixel), img->size.x, file_out) < img->size.x)
        {
            image_destroy(img);
            return WRITE_ERROR;
        }
        if (fwrite(offset_buffer, 1, horizontal_offset, file_out) < horizontal_offset)
        {
            image_destroy(img);
            return WRITE_ERROR;
        }
    }
    image_destroy(img);
    return WRITE_OK;
}
