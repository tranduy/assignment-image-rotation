#include "../include/image.h"
#include <stdlib.h>

struct image image_create(struct dimensions size_input)
{
    return (struct image){.size = size_input, .data = malloc(sizeof(struct pixel) * size_input.x * size_input.y)};
}

void image_destroy(struct image *image)
{
    free(image->data);
}
