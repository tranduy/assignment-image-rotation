#include "../include/image.h"
#include "../include/bmp.h"
#include "../include/file_action.h"
#include "../include/rotation_bmp.h"
#include "../include/util.h"
#include <stdio.h>
#define ARGC_ERROR_MORE "Слишком мало аргументов.\n"
#define ARGC_ERROR_LESS "Слишком много аргументов. \n"
#define INVALID_FILENAME_MESS "Ошибка открытия файла.\n"
#define OPEN_FILE_ERROR_MESS "Ошибка открытия файла.\n"
#define READ_FILE_ERROR_MESS "Ошибка при чтении из файла!\n"

void usage()
{
    fprintf(stderr, "Usage: ./rotate SRC_IMG NEW_IMG\n");
}

int main(int argc, char **argv)
{
    if (argc != 3)
        usage();
    if (argc < 3)
        err(ARGC_ERROR_LESS);
    if (argc > 3)
        err(ARGC_ERROR_MORE);

    struct image image = {.size.x = 0, .size.y = 0, .data = NULL};
    enum open_file_status open_status = open_file(argv[1], &image);
    if (open_status != OPEN_OK)
    {
        if (open_status == OPEN_INVALID_FILENAME)
        {
            err(INVALID_FILENAME_MESS);
        }
        if (open_status == OPEN_ERROR)
        {
            err(OPEN_FILE_ERROR_MESS);
        }
        err(READ_FILE_ERROR_MESS);
    }
    struct image rotated_image = {0};
    rotated_image = rotate(&image);
    // rotated_image = rotate(rotated_image);
    // rotated_image = rotate(rotated_image);
    enum close_file_status save_status = close_file(argv[2], &rotated_image);

    if (save_status != CLOSE_OK)
    {
        err(READ_FILE_ERROR_MESS);
    }
    return 0;
}
