#include "../include/rotation_bmp.h"
#include "dimensions.h"
struct image rotate(struct image *source)
{
    // x is width, y is height -> x= height, y= width
    struct dimensions size = {.x = source->size.y, .y = source->size.x};
    struct image new_image = image_create(size);

    // rotate anti-clockwise 90 degrees
    for (size_t row = 0; row < size.x; row++)
        for (size_t col = 0; col < size.y; col++)
            // new_image.data[col * size.x + row] = source.data[(row)*size.y + (size.y - col)];
            new_image.data[col * size.x + (size.x - 1 - row)] = source->data[row * size.y + col];

    image_destroy(source);
    return new_image;
}
