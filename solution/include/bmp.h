#ifndef BMP_H
#define BMP_H

#include "image.h"
#include <stdint.h>
#include <stdio.h>

#define BMP_VALID_BFTYPE 0x4D42
#define BMP_VALID_RESERVED 0
#define BMP_VALID_OFFSET 54
#define BMP_VALID_SIZE 40
#define BMP_VALID_PLANS 1
#define BMP_VALID_BITCOUNT 24
#define BMP_VALID_COMPRESSION 0
#define BMP_VALID_XPELS 2834
#define BMP_VALID_YPELS 2834
#define BMP_VALID_CLRU 0
#define BMP_VALID_CLRI 0

/* header of bmp image for Windows https://en.wikipedia.org/wiki/BMP_file_format#Bitmap_file_header */
#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

/*  deserializer */
enum read_status
{
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
    /* коды других ошибок  */
};
/*  deserializer */
enum read_status from_bmp(FILE *const file_in, struct image *img);

/*  serializer   */
enum write_status
{
    WRITE_OK = 0,
    WRITE_ERROR
};
/*  serializer   */
enum write_status to_bmp(FILE *const file_out, struct image *const img);

#endif
