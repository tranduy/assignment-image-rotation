#pragma once

#include <stddef.h>

struct dimensions
{
    size_t x;
    size_t y;
};
