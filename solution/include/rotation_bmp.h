#ifndef ROTATION_BMP_H
#define ROTATION_BMP_H

// #include <stdlib.h>
#include "image.h"
#include <stdio.h>

struct image rotate(struct image *source);

#endif /* ROTATION_BMP_H */
