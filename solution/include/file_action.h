#ifndef FILE_ACTION_H
#define FILE_ACTION_H
#include "image.h"
#include <stdio.h>
enum open_file_status
{
    OPEN_OK = 0,
    OPEN_INVALID_FILENAME,
    OPEN_ERROR
};
enum open_file_status open_file(char const *name, struct image *image);

enum close_file_status
{
    CLOSE_OK = 0,
    CLOSE_ERROR
};

enum close_file_status close_file(char const *name, struct image *image);

#endif // FILE_ACTION_H
